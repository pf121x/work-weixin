package cn.com.gwinfo.callback;

import cn.com.gwinfo.event.BaseEventData;

/**
 * 描述
 *
 * @author pfwang
 * date 2022-01-04 15:46
 **/
public interface CallBackChain {

    /**
     * 是否需要处理
     *
     * @param eventData       回调数据
     * @param applicationName 应用名
     * @return 是否匹配
     */
    boolean match(String applicationName, BaseEventData eventData);

    /**
     * 业务逻辑处理
     *
     * @param applicationName 应用名
     * @param eventData       回调数据
     */
    void handle(String applicationName, BaseEventData eventData);
}
