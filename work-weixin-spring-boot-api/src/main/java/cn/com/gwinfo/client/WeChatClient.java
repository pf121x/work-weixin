package cn.com.gwinfo.client;

import cn.com.gwinfo.client.config.WeChatConfiguration;
import cn.com.gwinfo.response.BaseResponse;
import cn.com.gwinfo.response.addressbook.*;
import cn.com.gwinfo.response.tool.AccessTokenResponse;
import cn.com.gwinfo.resquest.addressbook.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 描述 enjoy your life
 *
 * @author pfwang
 **/
@SuppressWarnings("all")
@FeignClient(name = "wechat", url = "${qywx.url:https://qyapi.weixin.qq.com}", path = "${qywx.public-path:cgi-bin}", configuration = WeChatConfiguration.class)
public interface WeChatClient {

    String HEAD_KEY = "app";
    String HEAD = HEAD_KEY + "={app}";
    String ACCESS_TOKEN = "access_token";
    String GET_TOKEN = "/gettoken";

    /**
     * 获取应用 access token
     *
     * @param corpId 企业号
     * @param secret 密匙
     * @return AccessTokenResponse
     */
    @GetMapping("gettoken")
    AccessTokenResponse getAccessToken(@RequestParam("corpid") String corpId, @RequestParam("corpsecret") String secret);

    /**
     * 创建成员
     *
     * @param request 请求参数
     * @param app     应用
     * @return BaseResponse
     */
    @PostMapping(value = "user/create", headers = HEAD)
    BaseResponse createUser(CreateUserRequest request, @RequestParam(HEAD_KEY) String app);

    /**
     * 读取成员信息
     *
     * @param app    应用名
     * @param userId 成员编号
     * @return WeComUserResponse
     */
    @GetMapping(value = "user/get", headers = HEAD)
    WeChatUserResponse getUser(@RequestParam("userid") String userId, @RequestParam(HEAD_KEY) String app);

    /**
     * 更新成员信息【根据userID更新，必传】
     *
     * @param app     应用名
     * @param request 更新请求参数
     * @return BaseResponse
     */
    @PostMapping(value = "user/update", headers = HEAD)
    BaseResponse updateUser(UpdateUserRequest request, @RequestParam(HEAD_KEY) String app);

    /**
     * 删除成员
     *
     * @param userId 成员编号
     * @param app    应用
     * @return BaseResponse
     */
    @GetMapping(value = "user/delete", headers = HEAD)
    BaseResponse deleteUser(@RequestParam("userid") String userId, @RequestParam(HEAD_KEY) String app);

    /**
     * 批量删除成员
     *
     * @param request 请求体
     * @param app     应用
     * @return BaseResponse
     */
    @PostMapping(value = "user/batchdelete", headers = HEAD)
    BaseResponse batchDelete(BatchDeleteUserRequest request, @RequestParam(HEAD_KEY) String app);

    /**
     * 获取部门成员[应用须拥有指定部门的查看权限。]
     *
     * @param app          应用名
     * @param departmentId 获取的部门id
     * @param fetchChild   1/0：是否递归获取子部门下面的成员
     * @return DepartmentUserResponse
     */
    @GetMapping(value = "user/simplelist", headers = HEAD)
    DepartmentUserResponse getDepartmentUser(@RequestParam("department_id") Integer departmentId, @RequestParam("fetch_child") Integer fetchChild, @RequestParam(HEAD_KEY) String app);

    /**
     * 获取部门成员详情[应用须拥有指定部门的查看权限。]
     *
     * @param app          应用名
     * @param departmentId 获取的部门id
     * @param fetchChild   1/0：是否递归获取子部门下面的成员
     * @return DepartmentUserResponse
     */
    @GetMapping(value = "user/list", headers = HEAD)
    DepartmentUserResponse getDepartmentUserDetail(@RequestParam("department_id") Integer departmentId, @RequestParam("fetch_child") Integer fetchChild, @RequestParam(HEAD_KEY) String app);

    /**
     * userid转openid
     *
     * @param request 请求体
     * @param app     应用名
     * @return ConvertUserIdOpenIdResponse
     */
    @PostMapping(value = "user/convert_to_openid", headers = HEAD)
    ConvertUserIdOpenIdResponse convertToOpenId(ConvertUserIdOpenIdRequest request, @RequestParam(HEAD_KEY) String app);

    /**
     * openid转userid
     *
     * @param request 请求体
     * @param app     应用名
     * @return ConvertUserIdOpenIdResponse
     */
    @PostMapping(value = "user/convert_to_userid", headers = HEAD)
    ConvertUserIdOpenIdResponse convertToUserId(ConvertUserIdOpenIdRequest request, @RequestParam(HEAD_KEY) String app);

    /**
     * 二次验证
     *
     * @param userId 用户编号
     * @param app    应用名
     * @return BaseResponse
     */
    @GetMapping(value = "user/authsucc", headers = HEAD)
    BaseResponse authSucc(@RequestParam("userid") String userId, @RequestParam(HEAD_KEY) String app);

    /**
     * 邀请成员
     *
     * @param request 请求体
     * @param app     应用名
     * @return BaseResponse
     */
    @PostMapping(value = "batch/invite", headers = HEAD)
    BaseResponse invite(InviteUserRequest request, @RequestParam(HEAD_KEY) String app);

    /**
     * 创建部门
     *
     * @param app     应用
     * @param request 请求体
     * @return CreateDepartmentResponse
     */
    @PostMapping(value = "department/create", headers = HEAD)
    CreateDepartmentResponse createDepartment(DepartmentRequest request, @RequestParam(HEAD_KEY) String app);

    /**
     * 更新部门
     *
     * @param request 请求体
     * @param app     应用名
     * @return BaseResponse
     */
    @PostMapping(value = "department/update", headers = HEAD)
    BaseResponse updateDepartment(DepartmentRequest request, @RequestParam(HEAD_KEY) String app);

    /**
     * 删除部门【注：不能删除根部门；不能删除含有子部门、成员的部门】
     *
     * @param id  企业微信部门id
     * @param app 应用名
     * @return BaseResponse
     */
    @GetMapping(value = "department/delete", headers = HEAD)
    BaseResponse deleteDepartment(@RequestParam(value = "id", required = false) Integer id, @RequestParam(HEAD_KEY) String app);

    /**
     * 权限说明：
     * 只能拉取token对应的应用的权限范围内的部门列表
     * <p>
     * 获取部门列表【获取指定部门及其下的子部门。 如果不填，默认获取全量组织架构】
     *
     * @param id  部门id
     * @param app 应用名
     * @return DepartmentListResponse
     */
    @GetMapping(value = "department/list", headers = HEAD)
    DepartmentListResponse departmentList(@RequestParam(value = "id", required = false) Integer id, @RequestParam(HEAD_KEY) String app);

}
