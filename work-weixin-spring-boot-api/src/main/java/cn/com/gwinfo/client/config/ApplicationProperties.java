package cn.com.gwinfo.client.config;

/**
 * 描述 应用授权信息配置
 *
 * @author pfwang
 **/

public class ApplicationProperties {

    /**
     * 应用密匙
     */
    private String secret;

    /**
     * 应用的编号
     */
    private String agentId;

    /**
     * 应用别名
     */
    private String applicationName;

    /**
     * 应用描述
     */
    private String applicationDesc;

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getApplicationDesc() {
        return applicationDesc;
    }

    public void setApplicationDesc(String applicationDesc) {
        this.applicationDesc = applicationDesc;
    }
}
