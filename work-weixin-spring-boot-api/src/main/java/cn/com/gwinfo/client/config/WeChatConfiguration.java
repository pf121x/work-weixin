package cn.com.gwinfo.client.config;

import cn.com.gwinfo.client.decoder.CustomJacksonDecoder;
import cn.com.gwinfo.client.interceptor.WeChatInterceptor;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;
import feign.jackson.JacksonEncoder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

/**
 * 描述 企业微信feign客户端配置
 *
 * @author pfwang
 **/
public class WeChatConfiguration {

    @Bean
    public Decoder decoder() {
        return new CustomJacksonDecoder();
    }

    @Bean
    public Encoder encoder() {
        return new JacksonEncoder();
    }

    @Bean
    public Feign.Builder builder(ApplicationContext applicationContext) {
        return new Feign.Builder().requestInterceptor(new WeChatInterceptor(applicationContext));
    }
}
