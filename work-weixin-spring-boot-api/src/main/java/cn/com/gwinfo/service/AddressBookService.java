package cn.com.gwinfo.service;

import cn.com.gwinfo.dto.addressbook.Department;
import cn.com.gwinfo.dto.addressbook.WeChatUser;
import cn.com.gwinfo.response.BaseResponse;
import cn.com.gwinfo.response.addressbook.*;
import cn.com.gwinfo.resquest.addressbook.*;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 企业微信通讯录管理
 *
 * @author pfwang
 * date 2019-04-04 14:22
 **/
@Service
public class AddressBookService extends AbstractBaseService {

    /**
     * 增量更新用户时上传的文件名称
     */
    private static final String ASYNC_BATCH_UPDATE_USER_FILE = "asyncBatchUpdateUser-%s.csv";

    /**
     * 全量覆盖用户时
     */
    private static final String FULL_COVER_USER_FILE = "fullCoverUser-%s.csv";

    /**
     * 批量更新用户
     */
    private static final String ASYNC_BATCH_UPDATE_USER = "asyncBatchUpdateUser";

    /**
     * 全量覆盖用户
     */
    private static final String FULL_COVER_USER = "fullCoverUser";

    /**
     * 更新用户时上传文件的title
     */
    private static final String ASYNC_BATCH_UPDATE_USER_FILE_TITLE = "姓名,帐号,手机号,邮箱,所在部门,职位,性别,是否部门内领导,排序,别名,地址,座机,禁用,禁用项说明：(0-启用;1-禁用)\n";


    private static final String FULL_COVER_DEPARTMENT_FILE_TITLE = "部门名称,部门ID,父部门ID,排序\n";

    /**
     * 全量覆盖部门
     */
    private static final String FULL_COVER_DEPARTMENT_FILE = "fullCoverUser-%s.csv";

    /**
     * 创建用户
     *
     * @param applicationName 应用名
     * @param request         请求
     */
    public final void createUser(CreateUserRequest request, String applicationName) {
        checkApplication(applicationName);
        if (Objects.nonNull(request)) {
            BaseResponse baseResponse = weChatClient.createUser(request, applicationName);
            if (isSuccess(baseResponse)) {
                logger.info("用户创建成功:{}", request);
            }
        }
    }

    /**
     * 读取成员信息
     *
     * @param userId          用户编号
     * @param applicationName 应用
     * @return WeChatUserResponse
     */
    public final WeChatUserResponse getUser(String userId, String applicationName) {
        checkApplication(applicationName);
        WeChatUserResponse weChatUserResponse = null;
        if (StringUtils.isNotEmpty(userId)) {
            weChatUserResponse = weChatClient.getUser(userId, applicationName);
            if (isSuccess(weChatUserResponse)) {
                if (logger.isInfoEnabled()) {
                    logger.info("读取到成员信息：{}", weChatUserResponse);
                }
            }
        }

        return weChatUserResponse;
    }

    /**
     * 更新成员信息
     *
     * @param request         更新信息
     * @param applicationName 应用
     */
    public final void updateUser(UpdateUserRequest request, String applicationName) {
        checkApplication(applicationName);
        if (Objects.nonNull(request) && StringUtils.isNotEmpty(request.getUserId())) {
            BaseResponse baseResponse = weChatClient.updateUser(request, applicationName);
            if (isSuccess(baseResponse)) {
                if (logger.isInfoEnabled()) {
                    logger.info("更新成员信息成功：userId-{},applicationName-{}", request.getUserId(), applicationName);
                }
            }
        }
    }

    /**
     * 删除成员
     *
     * @param userId          用户编号
     * @param applicationName 应用名称
     */
    public final void deleteUser(String userId, String applicationName) {
        checkApplication(applicationName);
        if (StringUtils.isNotEmpty(userId)) {
            BaseResponse response = weChatClient.deleteUser(userId, applicationName);
            if (isSuccess(response)) {
                if (logger.isInfoEnabled()) {
                    logger.info("删除成员成功：userId-{},applicationName-{}", userId, applicationName);
                }
            }
        }
    }

    /**
     * 批量删除成员
     *
     * @param request         请求
     * @param applicationName 应用名称
     */
    public final void batchDeleteUser(BatchDeleteUserRequest request, String applicationName) {
        checkApplication(applicationName);
        if (Objects.nonNull(request) && CollectionUtils.isNotEmpty(request.getUserIdList())) {
            BaseResponse response = weChatClient.batchDelete(request, applicationName);
            if (isSuccess(response)) {
                if (logger.isInfoEnabled()) {
                    logger.info("批量删除成员成功：{},applicationName-{}", request, applicationName);
                }
            }
        }
    }

    /**
     * 获取部门成员
     *
     * @param departmentId    企业微信端部门编号
     * @param fetchChild      是否获取子部门人员
     * @param applicationName 应用
     * @return WeChatUser
     */
    public final List<WeChatUser> getDepartmentUser(Integer departmentId, boolean fetchChild, String applicationName) {
        checkApplication(applicationName);
        List<WeChatUser> weChatUsers = new ArrayList<>(1);
        if (Objects.nonNull(departmentId)) {
            Integer getChild = fetchChild ? 1 : 0;
            DepartmentUserResponse departmentUserResponse = weChatClient.getDepartmentUser(departmentId, getChild, applicationName);
            if (isSuccess(departmentUserResponse)) {
                weChatUsers = departmentUserResponse.getWeChatUserList();

                if (logger.isInfoEnabled()) {
                    logger.info("获取部门成员成功：applicationName-{}--weChatUsers--{}", applicationName, weChatUsers);
                }
            }
        }

        return weChatUsers;
    }

    /**
     * 获取部门成员详情
     *
     * @param departmentId    企业微信端部门编号
     * @param fetchChild      是否获取子部门人员
     * @param applicationName 应用
     * @return WeChatUser
     */
    public final List<WeChatUser> getDepartmentUserDetail(Integer departmentId, boolean fetchChild, String applicationName) {
        checkApplication(applicationName);
        List<WeChatUser> weChatUsers = new ArrayList<>(1);
        if (Objects.nonNull(departmentId)) {
            Integer getChild = fetchChild ? 1 : 0;
            DepartmentUserResponse departmentUserResponse = weChatClient.getDepartmentUserDetail(departmentId, getChild, applicationName);
            if (isSuccess(departmentUserResponse)) {
                weChatUsers = departmentUserResponse.getWeChatUserList();

                if (logger.isInfoEnabled()) {
                    logger.info("获取部门成员详情成功：applicationName-{}--weChatUsers--{}", applicationName, weChatUsers);
                }
            }
        }

        return weChatUsers;
    }

    /**
     * userid转openid
     *
     * @param userId          用户编号
     * @param applicationName 应用名称
     * @return String
     */
    public final String convertToOpenId(String userId, String applicationName) {
        checkApplication(applicationName);
        String openId = null;
        if (StringUtils.isNotEmpty(userId)) {
            ConvertUserIdOpenIdResponse response = weChatClient.convertToOpenId(new ConvertUserIdOpenIdRequest(userId, null), applicationName);
            if (isSuccess(response)) {
                openId = response.getOpenId();
                if (logger.isInfoEnabled()) {
                    logger.info("获取openId成功：openid-{}----applicationName-{}", openId, applicationName);
                }
            }
        }

        return openId;
    }

    /**
     * openid转userid
     *
     * @param openId          用户编号
     * @param applicationName 应用名称
     * @return String
     */
    public final String convertToUserId(String openId, String applicationName) {
        checkApplication(applicationName);
        String userId = null;
        if (StringUtils.isNotEmpty(openId)) {
            ConvertUserIdOpenIdResponse response = weChatClient.convertToUserId(new ConvertUserIdOpenIdRequest(null, openId), applicationName);
            if (isSuccess(response)) {
                userId = response.getUserId();
                if (logger.isInfoEnabled()) {
                    logger.info("获取用户ID成功：userId-{}, applicationName-{}", userId, applicationName);
                }
            }
        }

        return userId;
    }

    /**
     * 二次验证
     *
     * @param userId          用户编号
     * @param applicationName 应用名称
     */
    public final void authSuccess(String userId, String applicationName) {
        checkApplication(applicationName);
        if (StringUtils.isNotEmpty(userId)) {
            BaseResponse response = weChatClient.authSucc(userId, applicationName);
            if (isSuccess(response)) {
                if (logger.isInfoEnabled()) {
                    logger.info("二次验证成功：userId-{},applicationName-{}", userId, applicationName);
                }
            }
        }
    }

    /**
     * 邀请成员
     *
     * @param request         请求
     * @param applicationName 应用名称
     */
    public final void invite(InviteUserRequest request, String applicationName) {
        checkApplication(applicationName);
        if (Objects.nonNull(request)) {
            if (CollectionUtils.isEmpty(request.getParty()) && CollectionUtils.isEmpty(request.getTag()) && CollectionUtils.isEmpty(request.getUser())) {
                return;
            }
            BaseResponse baseResponse = weChatClient.invite(request, applicationName);
            if (isSuccess(baseResponse)) {
                if (logger.isInfoEnabled()) {
                    logger.info("邀请成功：applicationName-{}", applicationName);
                }
            }
        }
    }

    /**
     * 创建部门
     *
     * @param request         请求
     * @param applicationName 应用名称
     * @return CreateDepartmentResponse
     */
    public final CreateDepartmentResponse createDepartment(DepartmentRequest request, String applicationName) {
        checkApplication(applicationName);
        CreateDepartmentResponse response = null;
        if (Objects.nonNull(request) && StringUtils.isNotEmpty(request.getName())) {
            response = weChatClient.createDepartment(request, applicationName);
            if (isSuccess(response)) {
                if (logger.isInfoEnabled()) {
                    logger.info("创建部门成功：部门编号-{},applicationName-{}", response.getId(), applicationName);
                }
            }
        }

        return response;
    }

    /**
     * 更新部门信息
     *
     * @param request         请求
     * @param applicationName 应用名称
     */
    public final void updateDepartment(DepartmentRequest request, String applicationName) {
        checkApplication(applicationName);
        if (Objects.nonNull(request) && Objects.nonNull(request.getId())) {
            BaseResponse response = weChatClient.updateDepartment(request, applicationName);
            if (isSuccess(response)) {
                if (logger.isInfoEnabled()) {
                    logger.info("更新部门成功：更新信息-{},applicationName-{}", request, applicationName);
                }
            }
        }
    }

    /**
     * 删除部门
     *
     * @param id              部门id
     * @param applicationName 应用名称
     */
    public final void deleteDepartment(Integer id, String applicationName) {
        checkApplication(applicationName);
        if (Objects.nonNull(id)) {
            BaseResponse response = weChatClient.deleteDepartment(id, applicationName);
            if (isSuccess(response)) {
                if (logger.isInfoEnabled()) {
                    logger.info("删除部门成功：部门编号-{},applicationName-{}", id, applicationName);
                }
            }
        }
    }

    /**
     * 拉取部门列表
     *
     * @param id              部门id
     * @param applicationName 应用名称
     * @return Department
     */
    public final List<Department> departmentList(Integer id, String applicationName) {
        checkApplication(applicationName);
        List<Department> departments = new ArrayList<>(1);
        DepartmentListResponse response = weChatClient.departmentList(id, applicationName);
        if (isSuccess(response)) {
            departments = response.getDepartments();
            if (logger.isInfoEnabled()) {
                logger.info("拉取部门列表成功：部门数量-{},applicationName-{}，departments--{}", departments.size(), applicationName, departments);
            }
        }

        return departments;
    }

}
