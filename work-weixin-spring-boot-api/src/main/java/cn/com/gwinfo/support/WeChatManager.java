package cn.com.gwinfo.support;

import cn.com.gwinfo.client.config.WeChatConfigurationProperties;
import cn.com.gwinfo.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 描述
 *
 * @author pfwang
 **/
@Component
public class WeChatManager {

    private final AddressBookService addressBookService;
    private final TokenService tokenService;
    private final WeChatConfigurationProperties properties;

    @Autowired
    public WeChatManager(AddressBookService addressBookService,
                         TokenService tokenService,
                         WeChatConfigurationProperties properties) {
        this.addressBookService = addressBookService;
        this.tokenService = tokenService;
        this.properties = properties;
    }

    /**
     * 通讯录管理服务
     *
     * @return AddressBookService
     */
    public AddressBookService addressBookService() {
        return addressBookService;
    }

    /**
     * 令牌管理服务
     *
     * @return TokenService
     */
    public TokenService tokenService() {
        return tokenService;
    }

    /**
     * 企业微信配置信息
     *
     * @return WeChatConfigurationProperties
     */
    public WeChatConfigurationProperties properties() {
        return properties;
    }



}
