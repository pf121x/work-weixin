package cn.com.gwinfo.autoconfigure;

import cn.com.gwinfo.client.config.WeChatConfigurationProperties;
import cn.com.gwinfo.support.CallBackManager;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.CacheManager;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.Collections;

/**
 * 描述
 *
 * @author pfwang
 * date 2022-01-04
 * @author pfwang
 */
@Configuration
@ComponentScan(basePackages = {"cn.com.gwinfo.service", "cn.com.gwinfo.support", "cn.com.gwinfo.aspect"})
@EnableConfigurationProperties(WeChatConfigurationProperties.class)
public class WeChatAutoConfiguration {

    @ConditionalOnMissingBean(CacheManager.class)
    @Bean
    public CacheManager cacheManager() {
        ConcurrentMapCacheManager concurrentMapCacheManager = new ConcurrentMapCacheManager();
        concurrentMapCacheManager.setAllowNullValues(true);
        concurrentMapCacheManager.setCacheNames(Collections.singletonList("qywx"));
        return concurrentMapCacheManager;
    }

    @Bean
    public CallBackManager callBackManager(WeChatConfigurationProperties properties) {
        CallBackManager.properties(properties.getCallbackList());
        return new CallBackManager();
    }
}
