package cn.com.gwinfo.dto.addressbook;

import java.util.List;

/**
 * 企业微信用户扩展信息
 *
 * @author pfwang
 **/
public class WeChatUserExtAttr {

    private List<ExtAttr> attrs;

    public List<ExtAttr> getAttrs() {
        return attrs;
    }

    public void setAttrs(List<ExtAttr> attrs) {
        this.attrs = attrs;
    }
}
