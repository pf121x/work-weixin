package cn.com.gwinfo.dto.checkin;

import java.util.List;

/**
 * 描述
 *
 * @author pfwang
 **/
public class CalOtTimeRule {
    private List<CalOtTimeRuleItem> items;

    public List<CalOtTimeRuleItem> getItems() {
        return items;
    }

    public void setItems(List<CalOtTimeRuleItem> items) {
        this.items = items;
    }
}
