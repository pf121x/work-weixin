package cn.com.gwinfo.dto.checkin;

import java.util.StringJoiner;

/**
 * 描述
 *
 * @author pfwang
 **/
public class CheckInException {
    private Integer count;
    private Integer duration;
    private Integer exception;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getException() {
        return exception;
    }

    public void setException(Integer exception) {
        this.exception = exception;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", CheckInException.class.getSimpleName() + "[", "]")
                .add("count=" + count)
                .add("duration=" + duration)
                .add("exception=" + exception)
                .toString();
    }
}
