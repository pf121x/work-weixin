package cn.com.gwinfo.dto.checkin;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * 描述
 *
 * @author pfwang
 **/
public class CheckInScheduleData {

    @JsonProperty("scheduleList")
    private List<CheckInScheduleDataDetail> scheduleDataDetails;

    public List<CheckInScheduleDataDetail> getScheduleDataDetails() {
        return scheduleDataDetails;
    }

    public void setScheduleDataDetails(List<CheckInScheduleDataDetail> scheduleDataDetails) {
        this.scheduleDataDetails = scheduleDataDetails;
    }
}
