package cn.com.gwinfo.dto.checkin;

/**
 * 描述
 *
 * @author pfwang
 **/
public class SpData {
    private String lang;
    private String text;

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
