package cn.com.gwinfo.dto.checkin;

import java.util.List;

/**
 * 描述
 *
 * @author pfwang
 **/
public class SpTitle {

    private List<SpData> data;

    public List<SpData> getData() {
        return data;
    }

    public void setData(List<SpData> data) {
        this.data = data;
    }
}
