package cn.com.gwinfo.dto.checkin;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 描述
 *
 * @author pfwang
 **/
public class TimeRule {

    @JsonProperty("offwork_after_time")
    private Integer offWorkAfterTime;

    @JsonProperty("onwork_flex_time")
    private Integer onWorkFlexTime;

    public Integer getOffWorkAfterTime() {
        return offWorkAfterTime;
    }

    public void setOffWorkAfterTime(Integer offWorkAfterTime) {
        this.offWorkAfterTime = offWorkAfterTime;
    }

    public Integer getOnWorkFlexTime() {
        return onWorkFlexTime;
    }

    public void setOnWorkFlexTime(Integer onWorkFlexTime) {
        this.onWorkFlexTime = onWorkFlexTime;
    }
}
