package cn.com.gwinfo.dto.externalcontact;

import cn.com.gwinfo.dto.message.MsgText;
import com.fasterxml.jackson.annotation.JsonProperty;
import cn.com.gwinfo.dto.message.MiniProgram;
import cn.com.gwinfo.dto.message.MsgImage;
import cn.com.gwinfo.dto.message.MsgLink;

/**
 * 描述
 *
 * @author pfwang
 **/
public class ConclusionData {
    private MsgText text;
    private MsgImage image;
    private MsgLink link;
    @JsonProperty("miniprogram")
    private MiniProgram miniProgram;

    public MsgText getText() {
        return text;
    }

    public void setText(MsgText text) {
        this.text = text;
    }

    public MsgImage getImage() {
        return image;
    }

    public void setImage(MsgImage image) {
        this.image = image;
    }

    public MsgLink getLink() {
        return link;
    }

    public void setLink(MsgLink link) {
        this.link = link;
    }

    public MiniProgram getMiniProgram() {
        return miniProgram;
    }

    public void setMiniProgram(MiniProgram miniProgram) {
        this.miniProgram = miniProgram;
    }
}
