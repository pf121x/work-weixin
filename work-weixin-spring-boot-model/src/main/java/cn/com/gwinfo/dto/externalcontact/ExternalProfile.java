package cn.com.gwinfo.dto.externalcontact;

import com.fasterxml.jackson.annotation.JsonProperty;
import cn.com.gwinfo.dto.addressbook.ExtAttr;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * 描述
 *
 * @author pfwang
 **/
public class ExternalProfile {
    @JsonProperty("external_attr")
    private List<ExtAttr> externalAttr;

    public List<ExtAttr> getExternalAttr() {
        return externalAttr;
    }

    public void setExternalAttr(List<ExtAttr> externalAttr) {
        this.externalAttr = externalAttr;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("externalAttr", externalAttr)
                .toString();
    }
}
