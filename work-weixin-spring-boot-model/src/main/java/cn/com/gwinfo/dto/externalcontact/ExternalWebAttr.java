package cn.com.gwinfo.dto.externalcontact;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 描述
 *
 * @author pfwang
 **/
public class ExternalWebAttr {
    private String url;
    private String title;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("url", url)
                .append("title", title)
                .toString();
    }
}
