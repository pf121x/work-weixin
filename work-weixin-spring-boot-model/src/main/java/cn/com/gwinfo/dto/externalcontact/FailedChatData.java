package cn.com.gwinfo.dto.externalcontact;

import cn.com.gwinfo.response.AbstractBaseResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 描述
 *
 * @author pfwang
 **/
public class FailedChatData extends AbstractBaseResponse {
    @JsonProperty("chat_id")
    private String chatId;

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }
}
