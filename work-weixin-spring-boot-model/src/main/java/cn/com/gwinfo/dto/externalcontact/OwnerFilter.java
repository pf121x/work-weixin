package cn.com.gwinfo.dto.externalcontact;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * 描述
 *
 * @author pfwang
 **/
public class OwnerFilter {

    @JsonProperty("userid_list")
    private List<String> userIdList;

    public List<String> getUserIdList() {
        return userIdList;
    }

    public void setUserIdList(List<String> userIdList) {
        this.userIdList = userIdList;
    }
}
