package cn.com.gwinfo.dto.message;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 描述
 *
 * @author pfwang
 **/
public class MsgLink {

    private String title;
    private String desc;
    private String url;
    @JsonProperty("picurl")
    private String picUrl;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }
}
