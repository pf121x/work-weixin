package cn.com.gwinfo.dto.message;

/**
 * 描述
 * <p>
 *
 * @author pfwang
 **/
public class MsgMarkdown {
    private String content;

    public MsgMarkdown(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
