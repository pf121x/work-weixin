package cn.com.gwinfo.dto.message;

import java.util.List;

/**
 * 描述
 *
 * @author pfwang
 **/
public class MsgMpNews {
    private List<MpNewsArticle> articles;

    public List<MpNewsArticle> getArticles() {
        return articles;
    }

    public void setArticles(List<MpNewsArticle> articles) {
        this.articles = articles;
    }
}
