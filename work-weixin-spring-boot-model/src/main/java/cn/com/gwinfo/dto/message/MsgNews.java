package cn.com.gwinfo.dto.message;

import java.util.List;

/**
 * 描述
 *
 * @author pfwang
 **/
public class MsgNews {
    private List<NewsArticle> articles;

    public List<NewsArticle> getArticles() {
        return articles;
    }

    public void setArticles(List<NewsArticle> articles) {
        this.articles = articles;
    }
}
