package cn.com.gwinfo.dto.message;

/**
 * 描述
 *
 * @author pfwang
 **/
public class MsgText {
    public MsgText(String content) {
        this.content = content;
    }

    public MsgText() {
    }

    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
