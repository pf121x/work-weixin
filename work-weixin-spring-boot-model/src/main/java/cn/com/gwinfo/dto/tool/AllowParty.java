package cn.com.gwinfo.dto.tool;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.StringJoiner;

/**
 * 描述
 *
 * @author pfwang
 **/
public class AllowParty {
    @JsonProperty("partyid")
    private List<Integer> partys;

    @Override
    public String toString() {
        return new StringJoiner(", ", AllowParty.class.getSimpleName() + "[", "]")
                .add("partys=" + partys)
                .toString();
    }
}
