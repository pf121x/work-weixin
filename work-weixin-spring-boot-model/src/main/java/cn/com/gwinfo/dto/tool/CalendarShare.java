package cn.com.gwinfo.dto.tool;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 描述
 *
 * @author pfwang
 **/
public class CalendarShare {

    @JsonProperty("userid")
    private String userId;
    private Integer readonly;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getReadonly() {
        return readonly;
    }

    public void setReadonly(Integer readonly) {
        this.readonly = readonly;
    }
}
