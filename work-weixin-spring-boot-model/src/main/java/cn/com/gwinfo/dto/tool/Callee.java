package cn.com.gwinfo.dto.tool;

/**
 * 描述
 *
 * @author pfwang
 **/
public class Callee {
    private String phone;
    private Long duration;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }
}
