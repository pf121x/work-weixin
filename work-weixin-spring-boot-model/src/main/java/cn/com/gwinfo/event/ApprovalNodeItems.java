package cn.com.gwinfo.event;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.List;

/**
 * 描述
 *
 * @author pfwang
 **/
@XmlAccessorType(XmlAccessType.FIELD)
public class ApprovalNodeItems {

    @XmlElement(name = "Item")
    private List<ApprovalNodeItem> approvalNodeItems;

    public List<ApprovalNodeItem> getApprovalNodeItems() {
        return approvalNodeItems;
    }

    public void setApprovalNodeItems(List<ApprovalNodeItem> approvalNodeItems) {
        this.approvalNodeItems = approvalNodeItems;
    }
}
