package cn.com.gwinfo.event;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.List;

/**
 * 描述
 *
 * @author pfwang
 **/
@XmlAccessorType(XmlAccessType.FIELD)
public class ApprovalNodes {

    @XmlElement(name = "ApprovalNode")
    private List<ApprovalNode> approvalNodes;

    public List<ApprovalNode> getApprovalNodes() {
        return approvalNodes;
    }

    public void setApprovalNodes(List<ApprovalNode> approvalNodes) {
        this.approvalNodes = approvalNodes;
    }
}
