package cn.com.gwinfo.event;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.StringJoiner;

/**
 * 描述
 *
 * @author pfwang
 **/
@XmlRootElement(name = "xml")
public class BatchJobResultEventData extends BaseEventData {

    @XmlElement(name = "BatchJob")
    private BatchJobData batchJob;

    public BatchJobData getBatchJob() {
        return batchJob;
    }

    public void setBatchJob(BatchJobData batchJob) {
        this.batchJob = batchJob;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", BatchJobResultEventData.class.getSimpleName() + "[", "]")
                .add("batchJob=" + batchJob)
                .toString();
    }
}
