package cn.com.gwinfo.event;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.List;

/**
 * 描述
 *
 * @author pfwang
 **/
@XmlAccessorType(XmlAccessType.FIELD)
public class ExtAttrData {

    @XmlElement(name = "Item")
    private List<ExtAttrItemData> itemData;

    public List<ExtAttrItemData> getItemData() {
        return itemData;
    }

    public void setItemData(List<ExtAttrItemData> itemData) {
        this.itemData = itemData;
    }
}
