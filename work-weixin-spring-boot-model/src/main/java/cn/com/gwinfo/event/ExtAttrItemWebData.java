package cn.com.gwinfo.event;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * 描述
 *
 * @author pfwang
 **/
@XmlAccessorType(XmlAccessType.FIELD)
public class ExtAttrItemWebData {

    @XmlElement(name = "Title")
    private String title;

    @XmlElement(name = "Url")
    private String url;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
