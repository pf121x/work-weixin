package cn.com.gwinfo.event;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 描述
 *
 * @author pfwang
 **/

@XmlRootElement(name = "xml")
public class OpenApprovalChangeEventData extends BaseEventData {

    @XmlElement(name = "ApprovalInfo")
    private ApprovalInfo approvalInfo;

    public ApprovalInfo getApprovalInfo() {
        return approvalInfo;
    }

    public void setApprovalInfo(ApprovalInfo approvalInfo) {
        this.approvalInfo = approvalInfo;
    }
}
