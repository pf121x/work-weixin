package cn.com.gwinfo.event;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * 描述
 *
 * @author pfwang
 **/
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlUserId {

    @XmlElement(name = "UserId")
    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
