package cn.com.gwinfo.exception;


import cn.com.gwinfo.enums.ErrorCode;

/**
 * 描述
 *
 * @author pfwang
 **/
public class WeChatException extends RuntimeException {
    private ErrorCode errorCode;

    public WeChatException(String message) {
        super(message);
    }


    public WeChatException(ErrorCode errorCode) {
        super(errorCode.getErrorDesc());
        this.errorCode = errorCode;
    }


    public WeChatException(String message, ErrorCode errorCode) {
        super(message);
        this.errorCode = errorCode;
    }


    public WeChatException(String message, Throwable cause, ErrorCode errorCode) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public WeChatException(Throwable cause, ErrorCode errorCode) {
        super(cause);
        this.errorCode = errorCode;
    }

    public WeChatException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, ErrorCode errorCode) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.errorCode = errorCode;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }
}
