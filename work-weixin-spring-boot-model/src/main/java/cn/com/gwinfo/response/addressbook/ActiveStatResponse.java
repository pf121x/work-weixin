package cn.com.gwinfo.response.addressbook;

import com.fasterxml.jackson.annotation.JsonProperty;
import cn.com.gwinfo.response.AbstractBaseResponse;

import java.util.StringJoiner;

/**
 * 获取企业活跃成员数
 *
 * @author pfwang
 **/
public class ActiveStatResponse extends AbstractBaseResponse {

    @JsonProperty("active_cnt")
    private Integer activeCnt;

    public Integer getActiveCnt() {
        return activeCnt;
    }

    public void setActiveCnt(Integer activeCnt) {
        this.activeCnt = activeCnt;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ActiveStatResponse.class.getSimpleName() + "[", "]")
                .add("activeCnt=" + activeCnt)
                .toString();
    }
}
