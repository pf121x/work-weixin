package cn.com.gwinfo.response.addressbook;

import com.fasterxml.jackson.annotation.JsonProperty;
import cn.com.gwinfo.response.AbstractBaseResponse;

import java.util.StringJoiner;

/**
 * 描述
 *
 * @author pfwang
 **/
public class AsyncJobResponse extends AbstractBaseResponse {

    @JsonProperty("jobid")
    private String jobId;

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", AsyncJobResponse.class.getSimpleName() + "[", "]")
                .add("jobId='" + jobId + "'")
                .toString();
    }
}
