package cn.com.gwinfo.response.addressbook;

import cn.com.gwinfo.response.AbstractBaseResponse;

import java.util.StringJoiner;

/**
 * 描述
 *
 * @author pfwang
 **/
public class CreateDepartmentResponse extends AbstractBaseResponse {
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    @Override
    public String toString() {
        return new StringJoiner(", ", CreateDepartmentResponse.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .toString();
    }
}
