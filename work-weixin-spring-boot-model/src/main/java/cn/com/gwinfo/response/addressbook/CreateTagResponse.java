package cn.com.gwinfo.response.addressbook;

import com.fasterxml.jackson.annotation.JsonProperty;
import cn.com.gwinfo.response.AbstractBaseResponse;

import java.util.StringJoiner;

/**
 * 描述
 *
 * @author pfwang
 **/
public class CreateTagResponse extends AbstractBaseResponse {

    @JsonProperty("tagid")
    private Integer tagId;

    public Integer getTagId() {
        return tagId;
    }

    public void setTagId(Integer tagId) {
        this.tagId = tagId;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", CreateTagResponse.class.getSimpleName() + "[", "]")
                .add("tagId=" + tagId)
                .toString();
    }
}
