package cn.com.gwinfo.response.addressbook;

import com.fasterxml.jackson.annotation.JsonProperty;
import cn.com.gwinfo.dto.addressbook.WeChatUser;
import cn.com.gwinfo.response.AbstractBaseResponse;

import java.util.List;

/**
 * 描述
 *
 * @author pfwang
 **/
public class DepartmentUserResponse extends AbstractBaseResponse {

    @JsonProperty("userlist")
    private List<WeChatUser> weChatUserList;

    public List<WeChatUser> getWeChatUserList() {
        return weChatUserList;
    }

    public void setWeChatUserList(List<WeChatUser> weChatUserList) {
        this.weChatUserList = weChatUserList;
    }
}
