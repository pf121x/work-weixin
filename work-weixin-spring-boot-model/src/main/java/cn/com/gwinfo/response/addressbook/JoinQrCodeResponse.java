package cn.com.gwinfo.response.addressbook;

import com.fasterxml.jackson.annotation.JsonProperty;
import cn.com.gwinfo.response.AbstractBaseResponse;

import java.util.StringJoiner;

/**
 * 获取加入企业二维码
 *
 * @author pfwang
 **/

public class JoinQrCodeResponse extends AbstractBaseResponse {

    @JsonProperty("join_qrcode")
    private String joinQrCode;

    public String getJoinQrCode() {
        return joinQrCode;
    }

    public void setJoinQrCode(String joinQrCode) {
        this.joinQrCode = joinQrCode;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", JoinQrCodeResponse.class.getSimpleName() + "[", "]")
                .add("joinQrCode='" + joinQrCode + "'")
                .toString();
    }
}
