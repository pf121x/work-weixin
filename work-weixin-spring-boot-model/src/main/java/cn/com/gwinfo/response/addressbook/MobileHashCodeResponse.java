package cn.com.gwinfo.response.addressbook;

import cn.com.gwinfo.response.AbstractBaseResponse;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 描述
 *
 * @author pfwang
 **/
public class MobileHashCodeResponse extends AbstractBaseResponse {
    private String hashcode;

    public String getHashcode() {
        return hashcode;
    }

    public void setHashcode(String hashcode) {
        this.hashcode = hashcode;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("hashcode", hashcode)
                .toString();
    }
}
