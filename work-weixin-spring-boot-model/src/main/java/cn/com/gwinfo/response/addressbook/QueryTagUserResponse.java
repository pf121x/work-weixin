package cn.com.gwinfo.response.addressbook;

import com.fasterxml.jackson.annotation.JsonProperty;
import cn.com.gwinfo.dto.addressbook.WeChatUser;
import cn.com.gwinfo.response.AbstractBaseResponse;

import java.util.List;

/**
 * 描述
 *
 * @author pfwang
 **/
public class QueryTagUserResponse extends AbstractBaseResponse {

    @JsonProperty("tagname")
    private String tagName;

    @JsonProperty("partylist")
    private List<Integer> partyList;

    @JsonProperty("userlist")
    private List<WeChatUser> userList;

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public List<Integer> getPartyList() {
        return partyList;
    }

    public void setPartyList(List<Integer> partyList) {
        this.partyList = partyList;
    }

    public List<WeChatUser> getUserList() {
        return userList;
    }

    public void setUserList(List<WeChatUser> userList) {
        this.userList = userList;
    }
}
