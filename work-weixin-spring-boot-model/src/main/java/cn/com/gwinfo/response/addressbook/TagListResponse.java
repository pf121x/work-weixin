package cn.com.gwinfo.response.addressbook;

import com.fasterxml.jackson.annotation.JsonProperty;
import cn.com.gwinfo.dto.addressbook.Tag;
import cn.com.gwinfo.response.AbstractBaseResponse;

import java.util.List;

/**
 * 描述
 *
 * @author pfwang
 **/
public class TagListResponse extends AbstractBaseResponse {

    @JsonProperty("taglist")
    private List<Tag> tagList;

    public List<Tag> getTagList() {
        return tagList;
    }

    public void setTagList(List<Tag> tagList) {
        this.tagList = tagList;
    }
}
