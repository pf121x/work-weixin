package cn.com.gwinfo.response.addressbook;

import com.fasterxml.jackson.annotation.JsonProperty;
import cn.com.gwinfo.response.AbstractBaseResponse;

import java.util.List;

/**
 * 描述
 *
 * @author pfwang
 **/
public class TagUserResponse extends AbstractBaseResponse {

    @JsonProperty("invalidlist")
    private String inValidList;

    @JsonProperty("invalidparty")
    private List<Integer> inValidParty;

    public String getInValidList() {
        return inValidList;
    }

    public void setInValidList(String inValidList) {
        this.inValidList = inValidList;
    }

    public List<Integer> getInValidParty() {
        return inValidParty;
    }

    public void setInValidParty(List<Integer> inValidParty) {
        this.inValidParty = inValidParty;
    }
}
