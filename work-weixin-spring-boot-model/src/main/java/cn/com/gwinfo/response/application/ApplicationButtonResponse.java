package cn.com.gwinfo.response.application;

import com.fasterxml.jackson.annotation.JsonProperty;
import cn.com.gwinfo.dto.tool.ApplicationButton;
import cn.com.gwinfo.response.AbstractBaseResponse;

import java.util.List;
import java.util.StringJoiner;

/**
 * 描述
 *
 * @author pfwang
 **/
public class ApplicationButtonResponse extends AbstractBaseResponse {

    @JsonProperty("button")
    private List<ApplicationButton> buttonList;

    public List<ApplicationButton> getButtonList() {
        return buttonList;
    }

    public void setButtonList(List<ApplicationButton> buttonList) {
        this.buttonList = buttonList;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ApplicationButtonResponse.class.getSimpleName() + "[", "]")
                .add("buttonList=" + buttonList)
                .toString();
    }
}
