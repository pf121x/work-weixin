package cn.com.gwinfo.response.externalcontact;

import com.fasterxml.jackson.annotation.JsonProperty;
import cn.com.gwinfo.dto.externalcontact.BatchExternalContact;
import cn.com.gwinfo.response.AbstractBaseResponse;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * 描述
 *
 * @author pfwang
 **/
public class BatchExternalContactResponse extends AbstractBaseResponse {

    @JsonProperty("external_contact_list")
    private List<BatchExternalContact> contactList;

    public List<BatchExternalContact> getContactList() {
        return contactList;
    }

    public void setContactList(List<BatchExternalContact> contactList) {
        this.contactList = contactList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("contactList", contactList)
                .toString();
    }
}
