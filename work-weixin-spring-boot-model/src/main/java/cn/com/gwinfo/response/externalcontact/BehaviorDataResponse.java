package cn.com.gwinfo.response.externalcontact;

import com.fasterxml.jackson.annotation.JsonProperty;
import cn.com.gwinfo.dto.externalcontact.BehaviorData;
import cn.com.gwinfo.response.AbstractBaseResponse;

import java.util.List;

/**
 * 描述
 *
 * @author pfwang
 **/
public class BehaviorDataResponse extends AbstractBaseResponse {

    @JsonProperty("behavior_data")
    private List<BehaviorData> behaviorData;

    public List<BehaviorData> getBehaviorData() {
        return behaviorData;
    }

    public void setBehaviorData(List<BehaviorData> behaviorData) {
        this.behaviorData = behaviorData;
    }
}
