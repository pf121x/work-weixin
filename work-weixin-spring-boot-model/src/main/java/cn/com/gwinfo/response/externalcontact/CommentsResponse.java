package cn.com.gwinfo.response.externalcontact;

import com.fasterxml.jackson.annotation.JsonProperty;
import cn.com.gwinfo.dto.externalcontact.Comment;
import cn.com.gwinfo.response.AbstractBaseResponse;

import java.util.List;

/**
 * 描述
 *
 * @author pfwang
 **/
public class CommentsResponse extends AbstractBaseResponse {
    @JsonProperty("comment_list")
    private List<Comment> commentList;
    @JsonProperty("like_list")
    private List<Comment> likeList;

    public List<Comment> getCommentList() {
        return commentList;
    }

    public void setCommentList(List<Comment> commentList) {
        this.commentList = commentList;
    }

    public List<Comment> getLikeList() {
        return likeList;
    }

    public void setLikeList(List<Comment> likeList) {
        this.likeList = likeList;
    }
}
