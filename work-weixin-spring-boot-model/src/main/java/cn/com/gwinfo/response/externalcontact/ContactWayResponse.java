package cn.com.gwinfo.response.externalcontact;

import com.fasterxml.jackson.annotation.JsonProperty;
import cn.com.gwinfo.dto.externalcontact.ContactWayDetail;
import cn.com.gwinfo.response.AbstractBaseResponse;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * @author pfwang
 */
public class ContactWayResponse extends AbstractBaseResponse {
    @JsonProperty("contact_way")
    private ContactWayDetail contactWayDetail;

    public ContactWayDetail getContactWayDetail() {
        return contactWayDetail;
    }

    public void setContactWayDetail(ContactWayDetail contactWayDetail) {
        this.contactWayDetail = contactWayDetail;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("contactWayDetail", contactWayDetail)
                .toString();
    }
}
