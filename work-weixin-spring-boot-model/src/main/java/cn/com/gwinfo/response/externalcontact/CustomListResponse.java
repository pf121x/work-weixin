package cn.com.gwinfo.response.externalcontact;

import com.fasterxml.jackson.annotation.JsonProperty;
import cn.com.gwinfo.response.AbstractBaseResponse;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * 描述
 *
 * @author pfwang
 **/
public class CustomListResponse extends AbstractBaseResponse {

    @JsonProperty("external_userid")
    private List<String> externalUserIds;

    public List<String> getExternalUserIds() {
        return externalUserIds;
    }

    public void setExternalUserIds(List<String> externalUserIds) {
        this.externalUserIds = externalUserIds;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("externalUserIds", externalUserIds)
                .toString();
    }
}
