package cn.com.gwinfo.response.externalcontact;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 描述
 *
 * @author pfwang
 **/
public class DeleteWelcomeTemplateResponse {
    @JsonProperty("template_id")
    private String templateId;
    @JsonProperty("agentid")
    private String agentId;

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }
}
