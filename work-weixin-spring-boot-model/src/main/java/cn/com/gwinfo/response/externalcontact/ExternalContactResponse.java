package cn.com.gwinfo.response.externalcontact;

import com.fasterxml.jackson.annotation.JsonProperty;
import cn.com.gwinfo.dto.externalcontact.ExternalContactDetail;
import cn.com.gwinfo.dto.externalcontact.FollowUserDetail;
import cn.com.gwinfo.response.AbstractBaseResponse;

import java.util.List;

/**
 * 描述 客户详情
 *
 * @author pfwang
 **/
public class ExternalContactResponse extends AbstractBaseResponse {

    @JsonProperty("external_contact")
    private ExternalContactDetail externalContact;

    @JsonProperty("follow_user")
    private List<FollowUserDetail> followUsers;

    public ExternalContactDetail getExternalContact() {
        return externalContact;
    }

    public void setExternalContact(ExternalContactDetail externalContact) {
        this.externalContact = externalContact;
    }

    public List<FollowUserDetail> getFollowUsers() {
        return followUsers;
    }

    public void setFollowUsers(List<FollowUserDetail> followUsers) {
        this.followUsers = followUsers;
    }
}
