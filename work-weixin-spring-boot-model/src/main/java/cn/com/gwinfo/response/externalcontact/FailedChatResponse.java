package cn.com.gwinfo.response.externalcontact;

import com.fasterxml.jackson.annotation.JsonProperty;
import cn.com.gwinfo.dto.externalcontact.FailedChatData;
import cn.com.gwinfo.response.AbstractBaseResponse;

import java.util.List;

/**
 * 描述
 *
 * @author pfwang
 **/
public class FailedChatResponse extends AbstractBaseResponse {

    @JsonProperty("failed_chat_list")
    private List<FailedChatData> failedChatList;

    public List<FailedChatData> getFailedChatList() {
        return failedChatList;
    }

    public void setFailedChatList(List<FailedChatData> failedChatList) {
        this.failedChatList = failedChatList;
    }
}
