package cn.com.gwinfo.response.externalcontact;

import cn.com.gwinfo.dto.message.MiniProgram;
import cn.com.gwinfo.dto.message.MsgImage;
import cn.com.gwinfo.dto.message.MsgLink;
import cn.com.gwinfo.dto.message.MsgText;
import cn.com.gwinfo.response.AbstractBaseResponse;

/**
 * 描述
 *
 * @author pfwang
 **/
public class GetWelcomeTemplateResponse extends AbstractBaseResponse {
    private MsgText text;
    private MsgImage image;
    private MsgLink link;
    private MiniProgram miniprogram;

    public MsgText getText() {
        return text;
    }

    public void setText(MsgText text) {
        this.text = text;
    }

    public MsgImage getImage() {
        return image;
    }

    public void setImage(MsgImage image) {
        this.image = image;
    }

    public MsgLink getLink() {
        return link;
    }

    public void setLink(MsgLink link) {
        this.link = link;
    }

    public MiniProgram getMiniprogram() {
        return miniprogram;
    }

    public void setMiniprogram(MiniProgram miniprogram) {
        this.miniprogram = miniprogram;
    }
}
