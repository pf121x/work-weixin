package cn.com.gwinfo.response.externalcontact;

import com.fasterxml.jackson.annotation.JsonProperty;
import cn.com.gwinfo.dto.externalcontact.GroupChatDetail;
import cn.com.gwinfo.response.AbstractBaseResponse;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 描述
 *
 * @author pfwang
 **/
public class GroupChatDetailResponse extends AbstractBaseResponse {

    @JsonProperty("group_chat")
    private GroupChatDetail detail;

    public GroupChatDetail getDetail() {
        return detail;
    }

    public void setDetail(GroupChatDetail detail) {
        this.detail = detail;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("detail", detail)
                .toString();
    }
}
