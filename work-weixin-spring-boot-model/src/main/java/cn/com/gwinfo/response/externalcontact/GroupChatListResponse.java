package cn.com.gwinfo.response.externalcontact;

import com.fasterxml.jackson.annotation.JsonProperty;
import cn.com.gwinfo.dto.externalcontact.GroupChat;
import cn.com.gwinfo.response.AbstractBaseResponse;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * 描述
 *
 * @author pfwang
 **/
public class GroupChatListResponse extends AbstractBaseResponse {

    @JsonProperty("group_chat_list")
    private List<GroupChat> groupChats;

    public List<GroupChat> getGroupChats() {
        return groupChats;
    }

    public void setGroupChats(List<GroupChat> groupChats) {
        this.groupChats = groupChats;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("groupChats", groupChats)
                .toString();
    }
}
