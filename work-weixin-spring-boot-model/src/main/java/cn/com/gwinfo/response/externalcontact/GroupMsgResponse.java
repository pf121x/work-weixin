package cn.com.gwinfo.response.externalcontact;

import com.fasterxml.jackson.annotation.JsonProperty;
import cn.com.gwinfo.dto.externalcontact.GroupMsg;
import cn.com.gwinfo.response.AbstractBaseResponse;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * 描述
 *
 * @author pfwang
 **/
public class GroupMsgResponse extends AbstractBaseResponse {

    @JsonProperty("group_msg_list")
    private List<GroupMsg> groupMsgList;

    public List<GroupMsg> getGroupMsgList() {
        return groupMsgList;
    }

    public void setGroupMsgList(List<GroupMsg> groupMsgList) {
        this.groupMsgList = groupMsgList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("groupMsgList", groupMsgList)
                .toString();
    }
}
