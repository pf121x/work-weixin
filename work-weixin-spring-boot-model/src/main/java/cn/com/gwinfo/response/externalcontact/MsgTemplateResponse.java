package cn.com.gwinfo.response.externalcontact;

import com.fasterxml.jackson.annotation.JsonProperty;
import cn.com.gwinfo.response.AbstractBaseResponse;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * 描述
 *
 * @author pfwang
 **/
public class MsgTemplateResponse extends AbstractBaseResponse {
    @JsonProperty("fail_list")
    private List<String> failList;
    @JsonProperty("msgid")
    private String msgId;

    public List<String> getFailList() {
        return failList;
    }

    public void setFailList(List<String> failList) {
        this.failList = failList;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("failList", failList)
                .append("msgId", msgId)
                .toString();
    }
}
