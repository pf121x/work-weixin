package cn.com.gwinfo.response.message;

import com.fasterxml.jackson.annotation.JsonProperty;
import cn.com.gwinfo.response.AbstractBaseResponse;

/**
 * 描述
 *
 * @author pfwang
 **/
public class CreateAppChatResponse extends AbstractBaseResponse {
    @JsonProperty("chatid")
    private String chatId;

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }
}
