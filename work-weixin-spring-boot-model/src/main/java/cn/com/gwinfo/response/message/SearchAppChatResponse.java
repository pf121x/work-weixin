package cn.com.gwinfo.response.message;

import com.fasterxml.jackson.annotation.JsonProperty;
import cn.com.gwinfo.dto.message.ChatInfo;
import cn.com.gwinfo.response.AbstractBaseResponse;

import java.util.StringJoiner;

/**
 * 描述
 *
 * @author pfwang
 **/
public class SearchAppChatResponse extends AbstractBaseResponse {
    @JsonProperty("chat_info")
    private ChatInfo chatInfo;

    public ChatInfo getChatInfo() {
        return chatInfo;
    }

    public void setChatInfo(ChatInfo chatInfo) {
        this.chatInfo = chatInfo;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", SearchAppChatResponse.class.getSimpleName() + "[", "]")
                .add("chatInfo=" + chatInfo)
                .toString();
    }
}
