package cn.com.gwinfo.response.tool;

import com.fasterxml.jackson.annotation.JsonProperty;
import cn.com.gwinfo.response.AbstractBaseResponse;

/**
 * 描述
 *
 * @author pfwang
 **/
public class AccessTokenResponse extends AbstractBaseResponse {

    @JsonProperty("access_token")
    private String accessToken;

    @JsonProperty("expires_in")
    private Integer expiresIn;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public Integer getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(Integer expiresIn) {
        this.expiresIn = expiresIn;
    }
}
