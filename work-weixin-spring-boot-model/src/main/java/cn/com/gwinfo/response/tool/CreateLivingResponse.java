package cn.com.gwinfo.response.tool;

import com.fasterxml.jackson.annotation.JsonProperty;
import cn.com.gwinfo.response.AbstractBaseResponse;

/**
 * 描述
 *
 * @author pfwang
 **/
public class CreateLivingResponse extends AbstractBaseResponse {

    @JsonProperty("livingid")
    private String livingId;

    public String getLivingId() {
        return livingId;
    }

    public void setLivingId(String livingId) {
        this.livingId = livingId;
    }
}
