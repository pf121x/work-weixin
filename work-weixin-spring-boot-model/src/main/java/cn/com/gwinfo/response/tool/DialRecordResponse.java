package cn.com.gwinfo.response.tool;

import com.fasterxml.jackson.annotation.JsonProperty;
import cn.com.gwinfo.dto.tool.DialRecord;
import cn.com.gwinfo.response.AbstractBaseResponse;

import java.util.List;

/**
 * 描述
 *
 * @author pfwang
 **/
public class DialRecordResponse extends AbstractBaseResponse {
    @JsonProperty("record")
    private List<DialRecord> records;

    public List<DialRecord> getRecords() {
        return records;
    }

    public void setRecords(List<DialRecord> records) {
        this.records = records;
    }
}
