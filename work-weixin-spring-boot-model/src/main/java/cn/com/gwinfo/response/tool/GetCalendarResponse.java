package cn.com.gwinfo.response.tool;

import com.fasterxml.jackson.annotation.JsonProperty;
import cn.com.gwinfo.dto.tool.CalendarData;
import cn.com.gwinfo.response.AbstractBaseResponse;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * 描述
 *
 * @author pfwang
 **/
public class GetCalendarResponse extends AbstractBaseResponse {
    @JsonProperty("calendar_list")
    private List<CalendarData> calendarList;

    public List<CalendarData> getCalendarList() {
        return calendarList;
    }

    public void setCalendarList(List<CalendarData> calendarList) {
        this.calendarList = calendarList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("calendarList", calendarList)
                .toString();
    }
}
