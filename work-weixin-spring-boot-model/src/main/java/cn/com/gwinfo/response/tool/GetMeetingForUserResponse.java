package cn.com.gwinfo.response.tool;

import com.fasterxml.jackson.annotation.JsonProperty;
import cn.com.gwinfo.response.AbstractBaseResponse;

import java.util.List;

/**
 * 描述
 *
 * @author pfwang
 **/
public class GetMeetingForUserResponse extends AbstractBaseResponse {

    @JsonProperty("meetingid_list")
    private List<String> meetingIdList;

    public List<String> getMeetingIdList() {
        return meetingIdList;
    }

    public void setMeetingIdList(List<String> meetingIdList) {
        this.meetingIdList = meetingIdList;
    }
}
