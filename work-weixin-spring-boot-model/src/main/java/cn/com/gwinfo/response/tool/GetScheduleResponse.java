package cn.com.gwinfo.response.tool;

import com.fasterxml.jackson.annotation.JsonProperty;
import cn.com.gwinfo.dto.checkin.ScheduleData;
import cn.com.gwinfo.response.AbstractBaseResponse;

import java.util.List;

/**
 * 描述
 *
 * @author pfwang
 **/
public class GetScheduleResponse extends AbstractBaseResponse {
    @JsonProperty("schedule_list")
    private List<ScheduleData> scheduleList;

    public List<ScheduleData> getScheduleList() {
        return scheduleList;
    }

    public void setScheduleList(List<ScheduleData> scheduleList) {
        this.scheduleList = scheduleList;
    }
}
