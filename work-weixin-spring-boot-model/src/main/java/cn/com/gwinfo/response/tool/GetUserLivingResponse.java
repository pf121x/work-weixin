package cn.com.gwinfo.response.tool;

import com.fasterxml.jackson.annotation.JsonProperty;
import cn.com.gwinfo.response.AbstractBaseResponse;

import java.util.List;

/**
 * 描述
 *
 * @author pfwang
 **/
public class GetUserLivingResponse extends AbstractBaseResponse {
    @JsonProperty("livingid_list")
    private List<String> livingIdList;

    public List<String> getLivingIdList() {
        return livingIdList;
    }

    public void setLivingIdList(List<String> livingIdList) {
        this.livingIdList = livingIdList;
    }
}
