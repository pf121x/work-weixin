package cn.com.gwinfo.response.tool;

import com.fasterxml.jackson.annotation.JsonProperty;
import cn.com.gwinfo.response.AbstractBaseResponse;

/**
 * 描述
 *
 * @author pfwang
 **/
public class LivingCodeResponse extends AbstractBaseResponse {
    @JsonProperty("living_code")
    private String livingCode;

    public String getLivingCode() {
        return livingCode;
    }

    public void setLivingCode(String livingCode) {
        this.livingCode = livingCode;
    }
}
