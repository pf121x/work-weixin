package cn.com.gwinfo.response.tool;

import com.fasterxml.jackson.annotation.JsonProperty;
import cn.com.gwinfo.dto.tool.LivingInfoData;
import cn.com.gwinfo.response.AbstractBaseResponse;

/**
 * 描述
 *
 * @author pfwang
 **/
public class LivingInfoResponse extends AbstractBaseResponse {

    @JsonProperty("living_info")
    private LivingInfoData livingInfo;

    public LivingInfoData getLivingInfo() {
        return livingInfo;
    }

    public void setLivingInfo(LivingInfoData livingInfo) {
        this.livingInfo = livingInfo;
    }
}
