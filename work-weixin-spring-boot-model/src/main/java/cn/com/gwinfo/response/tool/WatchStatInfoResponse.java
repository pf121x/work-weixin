package cn.com.gwinfo.response.tool;

import com.fasterxml.jackson.annotation.JsonProperty;
import cn.com.gwinfo.dto.tool.WatchStatInfo;
import cn.com.gwinfo.response.AbstractBaseResponse;

/**
 * 描述
 *
 * @author pfwang
 **/
public class WatchStatInfoResponse extends AbstractBaseResponse {

    private Integer ending;

    @JsonProperty("next_key")
    private String nextKey;

    @JsonProperty("stat_info")
    private WatchStatInfo watchStatInfo;

    public Integer getEnding() {
        return ending;
    }

    public void setEnding(Integer ending) {
        this.ending = ending;
    }

    public String getNextKey() {
        return nextKey;
    }

    public void setNextKey(String nextKey) {
        this.nextKey = nextKey;
    }

    public WatchStatInfo getWatchStatInfo() {
        return watchStatInfo;
    }

    public void setWatchStatInfo(WatchStatInfo watchStatInfo) {
        this.watchStatInfo = watchStatInfo;
    }
}
