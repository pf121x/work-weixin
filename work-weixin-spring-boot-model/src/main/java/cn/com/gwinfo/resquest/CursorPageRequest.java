package cn.com.gwinfo.resquest;

/**
 * 描述
 *
 * @author pfwang
 **/
public class CursorPageRequest {
    private String cursor;
    private Integer limit = 100;

    public String getCursor() {
        return cursor;
    }

    public void setCursor(String cursor) {
        this.cursor = cursor;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }
}
