package cn.com.gwinfo.resquest;

/**
 * 描述
 *
 * @author pfwang
 **/
public class OffsetPageRequest {
    private Integer offset;
    private Integer limit;

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }
}
