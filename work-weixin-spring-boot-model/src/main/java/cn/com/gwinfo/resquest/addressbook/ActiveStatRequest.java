package cn.com.gwinfo.resquest.addressbook;

import java.util.StringJoiner;

/**
 * 描述
 *
 * @author pfwang
 **/
public class ActiveStatRequest {

    private String date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ActiveStatRequest.class.getSimpleName() + "[", "]")
                .add("date='" + date + "'")
                .toString();
    }
}
