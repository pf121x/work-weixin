package cn.com.gwinfo.resquest.addressbook;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 描述
 *
 * @author pfwang
 **/
public class AddressBookExportRequest {

    @JsonProperty("tagid")
    private Long tagId;

    @JsonProperty("encoding_aeskey")
    private String encodingAesKey;

    @JsonProperty("block_size")
    private Integer blockSize;

    public String getEncodingAesKey() {
        return encodingAesKey;
    }

    public void setEncodingAesKey(String encodingAesKey) {
        this.encodingAesKey = encodingAesKey;
    }

    public Integer getBlockSize() {
        return blockSize;
    }

    public void setBlockSize(Integer blockSize) {
        this.blockSize = blockSize;
    }

    public Long getTagId() {
        return tagId;
    }

    public void setTagId(Long tagId) {
        this.tagId = tagId;
    }
}
