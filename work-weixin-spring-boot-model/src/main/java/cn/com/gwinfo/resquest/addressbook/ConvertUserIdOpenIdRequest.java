package cn.com.gwinfo.resquest.addressbook;

import cn.com.gwinfo.response.addressbook.ConvertUserIdOpenIdResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.StringJoiner;

/**
 * 描述
 *
 * @author pfwang
 **/
public class ConvertUserIdOpenIdRequest {

    @JsonProperty("userid")
    private String userId;

    @JsonProperty("openid")
    private String openId;

    public ConvertUserIdOpenIdRequest(String userId, String openId) {
        this.userId = userId;
        this.openId = openId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ConvertUserIdOpenIdResponse.class.getSimpleName() + "[", "]")
                .add("userId='" + userId + "'")
                .add("openId='" + openId + "'")
                .toString();
    }
}
