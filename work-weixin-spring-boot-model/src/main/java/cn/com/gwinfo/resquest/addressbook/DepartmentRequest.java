package cn.com.gwinfo.resquest.addressbook;


import cn.com.gwinfo.dto.addressbook.Department;

import java.util.StringJoiner;

/**
 * 描述
 *
 * @author pfwang
 **/
public class DepartmentRequest extends Department {
    @Override
    public String toString() {
        return new StringJoiner(", ", DepartmentRequest.class.getSimpleName() + "[", "]")
                .add(super.toString())
                .toString();
    }
}
