package cn.com.gwinfo.resquest.addressbook;

/**
 * 描述
 *
 * @author pfwang
 **/
public class MobileHashCodeRequest {
    private String mobile;
    private String state;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
