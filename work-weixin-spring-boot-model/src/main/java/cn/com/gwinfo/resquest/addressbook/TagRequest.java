package cn.com.gwinfo.resquest.addressbook;

import cn.com.gwinfo.dto.addressbook.Tag;

import java.util.StringJoiner;

/**
 * 描述
 *
 * @author pfwang
 **/
public class TagRequest extends Tag {
    @Override
    public String toString() {
        return new StringJoiner(", ", TagRequest.class.getSimpleName() + "[", "]")
                .add(super.toString())
                .toString();
    }
}
