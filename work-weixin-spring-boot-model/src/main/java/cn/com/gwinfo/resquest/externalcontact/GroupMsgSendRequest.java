package cn.com.gwinfo.resquest.externalcontact;

import cn.com.gwinfo.resquest.CursorPageRequest;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 描述
 *
 * @author pfwang
 **/
public class GroupMsgSendRequest extends CursorPageRequest {
    @JsonProperty("userid")
    private String userId;
    @JsonProperty("msgid")
    private String msgId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }
}
