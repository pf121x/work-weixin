package cn.com.gwinfo.resquest.externalcontact;

import cn.com.gwinfo.resquest.CursorPageRequest;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 描述
 *
 * @author pfwang
 **/
public class GroupMsgTaskRequest extends CursorPageRequest {

    @JsonProperty("msgid")
    private String msgId;

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

}
