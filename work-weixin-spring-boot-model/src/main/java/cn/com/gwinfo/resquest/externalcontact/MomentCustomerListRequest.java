package cn.com.gwinfo.resquest.externalcontact;

import cn.com.gwinfo.resquest.CursorPageRequest;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 描述
 *
 * @author pfwang
 **/
public class MomentCustomerListRequest extends CursorPageRequest {

    @JsonProperty("moment_id")
    private String momentId;

    @JsonProperty("userid")
    private String userId;

    public String getMomentId() {
        return momentId;
    }

    public void setMomentId(String momentId) {
        this.momentId = momentId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
