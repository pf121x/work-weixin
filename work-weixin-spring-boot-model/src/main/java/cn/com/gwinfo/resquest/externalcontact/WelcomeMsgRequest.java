package cn.com.gwinfo.resquest.externalcontact;

import cn.com.gwinfo.dto.message.MiniProgram;
import cn.com.gwinfo.dto.message.MsgImage;
import cn.com.gwinfo.dto.message.MsgLink;
import cn.com.gwinfo.dto.message.MsgText;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 描述
 *
 * @author pfwang
 **/
public class WelcomeMsgRequest {
    @JsonProperty("welcome_code")
    private String welcomeCode;
    private MsgText text;
    private MsgLink link;
    private MsgImage image;
    private MiniProgram miniprogram;

    public String getWelcomeCode() {
        return welcomeCode;
    }

    public void setWelcomeCode(String welcomeCode) {
        this.welcomeCode = welcomeCode;
    }

    public MsgText getText() {
        return text;
    }

    public void setText(MsgText text) {
        this.text = text;
    }

    public MsgLink getLink() {
        return link;
    }

    public void setLink(MsgLink link) {
        this.link = link;
    }

    public MsgImage getImage() {
        return image;
    }

    public void setImage(MsgImage image) {
        this.image = image;
    }

    public MiniProgram getMiniprogram() {
        return miniprogram;
    }

    public void setMiniprogram(MiniProgram miniprogram) {
        this.miniprogram = miniprogram;
    }
}
