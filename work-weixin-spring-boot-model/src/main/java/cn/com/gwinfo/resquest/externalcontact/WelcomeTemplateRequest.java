package cn.com.gwinfo.resquest.externalcontact;

import cn.com.gwinfo.dto.message.MiniProgram;
import cn.com.gwinfo.dto.message.MsgImage;
import cn.com.gwinfo.dto.message.MsgLink;
import cn.com.gwinfo.dto.message.MsgText;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 描述
 *
 * @author pfwang
 **/
public class WelcomeTemplateRequest {
    @JsonProperty("agentid")
    private Integer agentId;
    private MsgText text;
    private MsgLink link;
    private MsgImage image;
    private MiniProgram miniprogram;
    @JsonProperty("template_id")
    private String templateId;

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public MsgText getText() {
        return text;
    }

    public void setText(MsgText text) {
        this.text = text;
    }

    public MsgLink getLink() {
        return link;
    }

    public void setLink(MsgLink link) {
        this.link = link;
    }

    public MsgImage getImage() {
        return image;
    }

    public void setImage(MsgImage image) {
        this.image = image;
    }

    public MiniProgram getMiniprogram() {
        return miniprogram;
    }

    public void setMiniprogram(MiniProgram miniprogram) {
        this.miniprogram = miniprogram;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }
}
