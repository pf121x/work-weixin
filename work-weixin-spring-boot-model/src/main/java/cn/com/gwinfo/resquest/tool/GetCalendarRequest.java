package cn.com.gwinfo.resquest.tool;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * 描述
 *
 * @author pfwang
 **/
public class GetCalendarRequest {
    @JsonProperty("cal_id_list")
    private List<String> calIdList;

    public List<String> getCalIdList() {
        return calIdList;
    }

    public void setCalIdList(List<String> calIdList) {
        this.calIdList = calIdList;
    }
}
