package cn.com.gwinfo.resquest.tool;

import cn.com.gwinfo.resquest.OffsetPageRequest;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 描述
 *
 * @author pfwang
 **/
public class GetScheduleByCalendarRequest extends OffsetPageRequest {
    @JsonProperty("cal_id")
    private String calId;

    public String getCalId() {
        return calId;
    }

    public void setCalId(String calId) {
        this.calId = calId;
    }
}
