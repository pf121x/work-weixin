package cn.com.gwinfo.resquest.tool;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * 描述
 *
 * @author pfwang
 **/
public class GetScheduleRequest {
    @JsonProperty("schedule_id_list")
    private List<String> scheduleIdList;

    public List<String> getScheduleIdList() {
        return scheduleIdList;
    }

    public void setScheduleIdList(List<String> scheduleIdList) {
        this.scheduleIdList = scheduleIdList;
    }
}
