package cn.com.gwinfo.resquest.tool;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 描述
 *
 * @author pfwang
 **/
public class LivingShareRequest {

    @JsonProperty("ww_share_code")
    private String wwShareCode;

    public String getWwShareCode() {
        return wwShareCode;
    }

    public void setWwShareCode(String wwShareCode) {
        this.wwShareCode = wwShareCode;
    }
}
