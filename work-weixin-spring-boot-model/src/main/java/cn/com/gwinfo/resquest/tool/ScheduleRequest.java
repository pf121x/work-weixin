package cn.com.gwinfo.resquest.tool;

import cn.com.gwinfo.dto.checkin.ScheduleData;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 描述
 *
 * @author pfwang
 **/
public class ScheduleRequest {
    private ScheduleData schedule;
    @JsonProperty("agentid")
    private String agentId;

    public ScheduleData getSchedule() {
        return schedule;
    }

    public void setSchedule(ScheduleData schedule) {
        this.schedule = schedule;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }
}
